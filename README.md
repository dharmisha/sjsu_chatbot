# README #

### What is this repository for? ###
sjsu_chatbot is an interactive Bot designed in node.js using Natural Language processing that can answer your questions.

* scope of this Bot includes
   * SJSU
   * CMPE
* Node.js version 6.10.2

### How do I get set up? ###
1. Local machine installation

* git clone https://dharmisha@bitbucket.org/dharmisha/sjsu_chatbot.git
* cd sjsu_chatbot
* npm install
* node bin/run.js    #this command starts the server
* now you can ask questions to sjsu_chatbot from slack

2. Deployment using Docker

* docker run -it dharmishadoshi/sjsu_chatbot:v1

### List of related Questions that the bot can answer ###

* What is CMPE 273?
   * what are the topics covered in enterprise distributed systems?
* On what date is convocation?
   * how many tickets can we buy for convocation?
   * where to register for convocation?
* What are the core courses at SJSU??
   * What are the core courses of CMPE?
   * What are the core courses of SE?
* What is the final exam schedule?
   * When is the final exam of cmpe 273?
* what is the address of sjsu?
   * what is the address of engineering building?
   * what is the address of mlk?
* What are the different engineering majors offered in sjsu?
* What are the office hours of cmpe faculty?
   * What are the working hours of MLK?
* What are the prerequisite courses?
   * What are the prerequisite courses in cmpe?
   * What are the prerequisite courses in se?
* What are the specialization streams?
   * What are the specialization streams in cmpe?
   * What are the specialization streams in SE?
   * What are the specialization streams in EE?
* What is the population of sjsu?