'use strict';

const request = require('superagent');

module.exports.process = function process(intentData, cb) {

    if(intentData.intent[0].value !== 'university')
        return cb(new Error(`Expected university intent, got ${intentData.intent[0].value}`));

     return cb(false, 'San Jose State University');  
}