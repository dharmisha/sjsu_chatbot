'use strict';

const request = require('superagent');

module.exports.process = function process(intentData,message,cb) {

    if(intentData.intent[0].value !== 'greeting')
        return cb(new Error(`Expected greeting intent, got ${intentData.intent[0].value}`));
     return cb(false, "Hi <@" + message.user + ">! How may I help you?" );  
}